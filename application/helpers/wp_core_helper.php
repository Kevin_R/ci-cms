<?php

function the_post() {
	$CI =& get_instance();
	$uri = get_page();
	if($uri ==  $CI->post->get_blog_page_name($CI->option->get_blog_page_id())) {
		the_posts();
	} else if(is_home()) {
		
	} else {
		echo $CI->post->get_page(get_page())->post_content;
	}
	reset_prefix();
	// die();
}

function is_home() {
	$CI =& get_instance();
	$val = intval($CI->uri->total_segments());
	if($val > 0) {
		return false;
	}
	return true;
}

function the_posts() {
	$CI =& get_instance();
	$posts = $CI->post->get_posts();
	foreach ($posts as $post) {
		echo '<p>' . post_date($post->post_date) . '</p>';
		echo '<h2>' . $post->post_title . '</h2><br />';
		echo '<div>' . $post->post_content . '</div>';
	}
}

function post_date($date) {
	$CI =& get_instance();
	$d = time($date);
	$CI->load->helper('date');
	$datestring = "%F %j%S, %Y";
	return mdate($datestring, $d);
}

function get_page() {
	$CI =& get_instance();
	$uri = $CI->uri->segment(1);
	return $uri;
}

function the_title() {	
	$uri = get_page();
	$CI =& get_instance();
	$CI->load->library('wordpress');
	echo $CI->wordpress->get_titles();
	// $id = get_front_page_id();
	// if(is_home()) {
	// 	echo 'ruin my day' . $id;
	// 	// echo get_front_page_title();
	// 	// echo $CI->post->get_front_page_title($CI->option->get_front_page_id());
	// 	// echo $CI->post->get_front_page($CI->option->get_front_page_id())->post_title;
	// } else {
	// 	echo $CI->post->get_post_title($CI->uri->segment(1));
	// }
}

function get_front_page_title()
{
	$CI =& get_instance();
	return $CI->post->get_front_page_title(get_front_page_id());
}

function get_front_page_id()
{
	$CI =& get_instance();
	return intval($CI->option->get_front_page_id());
}

function reset_prefix() {
	$CI =& get_instance();
	$CI->post->reset_prefix();
	die();
}

?>