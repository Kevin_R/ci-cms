<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wp
{
	protected $_ci;
	protected $_id;

	public function __construct()
	{
        $this->_ci =& get_instance();
        $this->_ci->load->model('wordpress/option_model', 'option');
        $this->_ci->load->model('wordpress/post_model', 'post');
	}

	public function the_content($slug = NULL)
	{
		// $res = new StdClass;
		// $res = $this->_ci->post->select('post_title, post_content')->find_by('post_name', $slug);
		// $res->post_title = $q['post_title'];
		$res = $this->_ci->post->the_content($slug);
		print_r($res);
	}

	public function is_home()
	{
		if($this->_ci->uri->total_segments() == 0)
		{
			//$this->_id = $this->_ci->option->get_front_page_id();
			return true;
		}
		return false;
	}

	public function is_blog()
	{
		if($this->_ci->option->get_blog_page_id() == $this->_ci->post->select('ID')->find_by('post_name', $this->_ci->uri->segment($this->_ci->uri->total_segments()))) {
			return true;
		}
		return false;
	}
	

}

/* End of file Wordpress.php */
/* Location: .//C/xampp/htdocs/ci-cms/bonfire/libraries/Wordpress.php */
