<?php
class My_Hooks {
   
    private $CI;
    
    public function __construct()
    {
        $this->CI =& get_instance();
    }
    
    public function get_username()
    {
        if(!isset($this->CI->session))
        {
            $this->CI->load->library('session');
        }
        $username = $this->CI->session->userdata('username');
        return $username;
    }
}