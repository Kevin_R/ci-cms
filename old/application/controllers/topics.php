<?php

/*
 * class Topics
 */

class Topics extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('topic_model');
        $this->load->library('pagination');
    }

    function index()
    {
        //pagination
        $this->config->load('pagination');
        $config = array();
        $config["base_url"] = base_url() . "topics";
        $config["total_rows"] = $this->topic_model->record_count();
        $config["per_page"] = 5;
        $config["use_page_numbers"] = TRUE;
        $config["uri_segment"] = 2;
        $this->pagination->initialize($config);
        $offset = ($this->uri->segment(2)) ? ($this->uri->segment(2) * $config["per_page"]) - $config["per_page"] : 0;

        $data['topics'] = $this->topic_model->limit($config["per_page"], $offset)->order_by('date', 'desc')->get_all();
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'topics_view';

        $this->load->view('template', $data);
    }

    function topic($id)
    {
        $data['query'] = $this->topic_model->get_by_id($id);
        $data['main_content'] = 'page_view';
        $this->load->view('template', $data);
    }
}