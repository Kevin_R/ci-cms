<?php

/*
 * class Page
 */

class Page extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    function index()
    {
        $data['main_content'] = 'page_view';
        $this->load->view('template', $data);
    }
}