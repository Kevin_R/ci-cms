<?php

/*
 * class Test
 */

class Test extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('topic_model');
        $this->load->library('pagination');
    }

    function index()
    {
        $data['users'] = $this->user_model->get_all();
        $data['sessid'] = $this->user_model->get_by_sess_id();
        $data['sessdata'] = $this->user_model->get_session_data();
        $data['main_content'] = 'test_view';
        $this->load->view('template', $data);
    }

    function query()
    {
        $this->db->from('px_users');
        $this->db->join('px_profiles', 'px_profiles.id = px_users.id');
        $this->db->join('px_files', 'px_files.user_id = px_users.id');
        $this->db->join('px_topics', 'px_topics.user_id = px_users.id');
        $this->db->order_by('px_topics.date', 'desc');

        $query = $this->db->get();
        echo '<pre>';
        print_r($query->result_array());
        echo '</pre>';
    }

    function datequery()
    {
        $months = " EXTRACT(MONTH FROM date) as month,\n";
        $years = " EXTRACT(YEAR FROM date) as year,\n";
        $time = " EXTRACT(DAY_MINUTE FROM date) as time,\n";
        $sql1 =
            "SELECT\n"
                . $months
                . $years
                . $time
                . " date\n"
                . " FROM px_topics\n"
                . " GROUP BY\n"
                . " month,\n"
                . " year\n"
                . " ORDER BY\n"
                . " date DESC";

        $data['query1'] = $this->db->query($sql1)->result_array();

        $sql2 =
            "SELECT\n"
            . $months
            . $years
            . " date,\n"
            . " title\n"
            . " FROM px_topics\n"
            . " ORDER BY\n"
            . " date DESC\n";

        $data['query2'] = $this->db->query($sql2)->result_array();


        //$data['months'] = $months;
        //$data['years'] = $years;

        //return $query->result_array();
        echo '<pre>';
        print_r($data['query2']);
        echo '</pre>';

        $this->load->view('datequery_view', $data);
    }

    function paged()
    {
        $this->config->load('pagination');
        $config = array();
        $config["base_url"] = base_url() . "test/paged";
        $config["total_rows"] = $this->topic_model->record_count();
        $config["per_page"] = 5;
        //$config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        //$config["next_tag_open"] = '<div id="endless-scroll" class="next">';
        //$config["next_tag_close"] = '</div>';
        //$choice = $config["total_rows"] / $config["per_page"];
        //$config["num_links"] = round($choice);
        //$config["num_links"] = "3";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) * $config["per_page"]) - $config["per_page"] : 0;
        $data["results"] = $this->topic_model->limit($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data["main_content"] = 'test2_view';

        $this->load->view('template', $data);
    }

    function test()
    {
        $this->layout = false;
        //$this->load->model('topic_model');
        $row = $this->topic_model->get(1);
        echo '<h1>' . $row->title . '</h1>';
        $result = $this->topic_model->get_all();
        echo '<pre>';
        print_r($result);
        echo '</pre>';
    }
}