<?php

/*
 * class Calendar
 */

class Calendar extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('event_model');
    }

    function index()
    {
        $data['main_content'] = 'calendar_view';
        $data['username'] = $this->session->userdata('username');
        //$data['events'] = $this->crud_model->display_events();
        $data['events'] = $this->event_model->order_by('date', 'desc')->get_all();
        $this->load->view('template', $data);
    }
}