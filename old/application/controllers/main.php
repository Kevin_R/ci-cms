<?php

/*
 * class Main
 */

class Main extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('main_view', array('error' => ''));
    }

    public function upload() {
        $this->load->helper('string');

        $config['upload_path'] = UPLOADS_PATH;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        //$config['file_name'] = random_string('alnum', 10) . '.jpg';
        $config['encrypt_name'] = TRUE;
        $config['max_filename'] = 10;
        $this->load->library('upload', $config);
        //$this->upload->initialize($config);

        if($this->upload->do_upload('image')) {
            $file_data = $this->upload->data();
            $data['filename'] = '../' . UPLOADS_PATH . $file_data['file_name'];
            $this->db->insert('px_files', $data);
            print_r($data);
            $this->load->view('success_msg', $data);
        } else {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('main_view', $error);
        }
    }
}