<?php

/*
 * class Login
 */

class Login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->load->view('login_view');
    }

    function validate_cred()
    {
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

        $this->load->model('user_model');
        $password = sha1($password);
        $query = $this->user_model->validate_user($username, $password);

        /*
         *  Worth exploring more advanced security
         *  While this isn't a working model using encode before storing
         *  And decode on retrieval, just might work...
         *
         *  $username = $this->security->xss_clean($this->input->post('username'));
         *  $password = $this->security->xss_clean($this->input->post('password'));
         *  $shapass = sha1($password);
         *  $this->load->library('encrypt');
         *  $password = $this->encrypt->encode($shapass);
         *  $this->load->model('members_model');
         *  $query = $this->members_model->validate($username, $this->encrypt->decode($password));
         *
         */

        if($query)
        {
            redirect('/');
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }
}