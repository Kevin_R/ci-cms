<?php

/*
 * class Admin
 */

class Admin extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        $admin = IS_ADMIN;

        if(!$admin)
        {
            redirect('logout');
        }

        $this->load->model('user_model');
    }

    function index()
    {
        $data['main_content'] = 'admin_view';
        $this->load->view('template', $data);
    }

    function add_user()
    {
        $this->crud_model->create_user();
        redirect('admin/users');
    }

    function users()
    {
        $this->load->library('table');

        $records = $this->crud_model->list_users();
	$tmpl = array(
	    'table_open' => '<table class="table">'
	);

	$this->table->set_template($tmpl);
	$data['table'] = $this->table->generate($records);

        $data['main_content'] = 'users_view';
        $this->load->view('template', $data);
    }
}