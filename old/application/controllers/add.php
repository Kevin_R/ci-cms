<?php

/*
 * class Add
 */

class Add extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        redirect('/');
    }

    function event()
    {
        $this->load->library('table');
	$this->load->model('user_model');

	$records = $this->user_model->as_array()->get_by_column();
	$tmpl = array(
	    'table_open' => '<table class="table example" id="example">',
	);
        $theading = array('username','email', 'role');

        $this->table->set_heading($theading);
	$this->table->set_template($tmpl);
	$data['table'] = $this->table->generate($records);

        $data['username'] = $this->session->userdata('username');
        $data['main_content'] = 'addevent_view';

        //echo '<pre>';
        //print_r($records);
        //echo '</pre>';

        $this->load->view('template', $data);
    }

    function topic()
    {
        $data['username'] = $this->session->userdata('username');
        $data['main_content'] = 'addtopic_view';
        $this->load->view('template', $data);
    }

    function new_topic()
    {
        $this->crud_model->create_topic();
    }

    function new_event()
    {
        $this->crud_model->create_event();
    }
}