<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MY_Controller {

	public function __construct()
	{
		$this->models = array('post',);
		$this->helpers = array('custom_helper', 'wp_helper');
		parent::__construct();		
	}

	// public function site_url()
	// {
	// 	$this->wp->get_site_url();
	// }

	public function index()
	{
		// $custom_image = get_loader();
		$this->data = array('posts' => $this->post->get_blog_entries(),);
		// echo '<pre>';
		// print_r($this->data);
		// echo '</pre>';		
		$this->layout = FALSE;
		$this->view = 'blog/index';
	}

}

/* End of file  */
/* Location: ./application/controllers/ */