<?php

/*
 * class Profile
 */

class Profile extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    function index($username = NULL)
    {
        if(isset($username))
        {
            $data['query'] = $this->user_model->get_profile($username);
        } else {
            $data['query'] = $this->user_model->get_user();
        }

        if(empty($data['query'])) {
            show_404();
        } else {
            $data['main_content'] = 'profile_view';
            $data['username'] = CURRENT_USERNAME;
            $this->load->view('template', $data);
        }
    }

    function edit()
    {
        $data['main_content'] = 'editprofile_view';
        $data['query'] = $this->user_model->get_user();
        $this->load->view('template' ,$data);
    }

    function update()
    {
        $id = CURRENT_USER_ID;
        $this->user_model->check_user($id);
    }
}