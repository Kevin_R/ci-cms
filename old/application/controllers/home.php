<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	// protected $models = array('option');

	public function __construct()
	{
		$this->models = array('option');
		parent::__construct();
	}

	public function index()
	{
		$this->layout = FALSE;
		$this->data = array('site_title' => $this->option->get_title(), );
		//print_r($this->data);
		$this->view = 'home/index';//array('home/index', $this->data);
		// $this->load->view('home_view', $data);
	}

}

/* End of file  */
/* Location: ./application/controllers/ */