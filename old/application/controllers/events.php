<?php

/*
 * class Events
 */

class Events extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    function index($id = NULL)
    {        
        $data['query'] = $this->crud_model->get_event($id);
        $data['main_content'] = 'page_view';
        
        $this->load->view('template', $data);
    }
}