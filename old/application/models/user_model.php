<?php

class User_Model extends MY_Model {

    public $db_table_name = 'px_users';
    public $username =  'username';
    public $id = CURRENT_USER_ID;

    //protected $return_type = 'array';
    protected $before_get = '';

    function __construct()
    {
        parent::__construct();
    }

    /* Generic Queries */

    //public function get_all()
    //{
    //    $query = $this->db->get($this->db_table_name);
    //    $res = $query->result_array();
    //
    //    return $res;
    //}

    public function get_by_column()
    {
        $this->before_get = $this->db->select('username, email, role');
        $query = $this->get_all();
        return $query;
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->db_table_name);
        $row = $query->row_array();

        return $row;
    }

    public function get_by_name($name)
    {
        $this->db->where($this->username, $name);
        $query = $this->db->get($this->db_table_name);
        $row = $query->row_array();

        return $row;
    }

    public function get_session_data()
    {
        $sessionData = $this->session->all_userdata();
        return $sessionData;
    }

    /* Custom queries */

    function is_admin()
    {
        $id = $this->session->userdata('id');

        $this->db->select('role');
        $this->db->from($this->db_table_name);
        $this->db->where('id', $id);

        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $row = $query->row();
            $role = $row->role;
            if($role == 1) {
                return true;
            }
        }

        return false;
    }

    function get_by_sess_id()
    {
        $this->db->select('*');
        $this->db->from('px_users');
        $this->db->where('px_users.id', $this->id);
        $this->db->join('px_profiles', 'px_profiles.id = px_users.id');

        $query = $this->db->get();

        return $query->row_array();
    }

    function get_current_username()
    {
        $id = $this->session->userdata('id');
        $this->db->select('username');
        $this->db->from('px_users');
        $this->db->where('id', $id);

        $query = $this->db->get();

        return $query->row_array();
    }

    function get_user()
    {
        $id = $this->session->userdata('id');


        $this->db->select('*');
        $this->db->from('px_users');
        $this->db->where('px_users.id', $id);
        $this->db->join('px_profiles', 'px_profiles.id = px_users.id');
        $this->db->join('px_files', 'px_files.user_id = px_users.id');

        $query = $this->db->get();

        return $query->row_array();
    }

    function get_profile($username)
    {
        $this->db->select('*');
        $this->db->from('px_users');
        $this->db->where('px_users.username', $username);
        $this->db->join('px_profiles', 'px_users.id = px_profiles.id');
        $this->db->join('px_files', 'px_users.id = px_files.user_id');

        $query = $this->db->get();

        return $query->row_array();
    }

    function create_user_profile()
    {
        $id = $this->session->userdata('id');

        $data1 = array(
            'id' => $id,
            'fname' => $this->input->post('fname'),
            'sname' => $this->input->post('sname'),
            'wannabee' => $this->input->post('wannabee'),
            'specialty' => $this->input->post('specialty'),
            'website' => $this->input->post('website'),
            'phone' => $this->input->post('phone'),
            'about' => $this->input->post('editor1')
        );

        $data2 = array('email' => $this->input->post('email'));
        $data3['user_id'] = $id;

        $this->db->where('px_profiles.id', $id);
        $this->db->insert('px_profiles', $data1);

        $this->db->where('px_users.id', $id);
        $this->db->insert('px_users', $data2);

        $this->db->insert('px_files', $data3);

        redirect('/profile/edit');
    }

    function update_user_profile()
    {
        //$id = $this->session->userdata('id');
        $id = CURRENT_USER_ID;

        $this->load->helper('string');

        $config['upload_path'] = UPLOADS_PATH;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        //$this->upload->initialize($config);

        if($this->upload->do_upload('image')) {
            $file_data = $this->upload->data();

        } else {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('main_view', $error);
        }

        $data = array(
            'fname' => $this->input->post('fname'),
            'sname' => $this->input->post('sname'),
            'wannabee' => $this->input->post('wannabee'),
            'specialty' => $this->input->post('specialty'),
            'website' => $this->input->post('website'),
            'phone' => $this->input->post('phone'),
            'about' => $this->input->post('editor1'),
        );

        $data2 = array(
            'email' => $this->input->post('email')
        );

        $data3['filename'] = '../' . UPLOADS_PATH . $file_data['file_name'];

        $this->db->from('px_profiles');
        $this->db->where('id', $id);

        $this->db->set($data);
        $this->db->update();

        $this->db->from('px_users');
        $this->db->where('id', $id);

        $this->db->set($data2);
        $this->db->update();

        $this->db->from('px_files');
        $this->db->where('user_id', $id);

        $this->db->set($data3);
        $this->db->update();

        //$this->db->join('px_files', 'px_files.user_id = px_users.id');

        redirect('profile');
    }

    function check_user($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('px_profiles');
        if($query->num_rows() > 0)
        {
            $this->update_user_profile();
        } else {
            $this->create_user_profile();
        }
    }

    function validate_user($username, $password)
    {
        //grab user input
        $this->db->from('px_users');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->join('px_profiles', 'px_users.id = px_profiles.id');

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            $row = $query->row();
            $data = array(
                'username' => $row->username,
                'logged_in' => TRUE,
                'id' => $row->id,
                'role' => $row->role,
                'fname' => $row->fname,
                'email' => $row->email
            );
            $this->session->set_userdata($data);
            return true;
        }

        return false;
    }
}