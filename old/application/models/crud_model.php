<?php

class Crud_Model extends CI_Model {

    var $email = '';

    function __construct()
    {
        parent::__construct();
    }

    /*
     *  User Functions
     */

    function is_admin()
    {
        $id = $this->session->userdata('id');

        $this->db->select('role');
        $this->db->from('px_users');
        $this->db->where('id', $id);

        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $row = $query->row();
            $role = $row->role;
            if($role == 1) {
                return true;
            }
        }

        return false;
    }

    function get_current_username()
    {
        $id = $this->session->userdata('id');
        $this->db->select('username');
        $this->db->from('px_users');
        $this->db->where('id', $id);

        $query = $this->db->get();

        return $query->row_array();
    }

    function get_user()
    {
        $id = $this->session->userdata('id');


        $this->db->select('*');
        $this->db->from('px_users');
        $this->db->where('px_users.id', $id);
        $this->db->join('px_profiles', 'px_profiles.id = px_users.id');
        $this->db->join('px_files', 'px_files.user_id = px_users.id');

        $query = $this->db->get();

        return $query->row_array();
    }

    function get_profile($username)
    {
        $this->db->select('*');
        $this->db->from('px_users');
        $this->db->where('px_users.username', $username);
        $this->db->join('px_profiles', 'px_users.id = px_profiles.id');
        $this->db->join('px_files', 'px_users.id = px_files.user_id');

        $query = $this->db->get();

        return $query->row_array();
    }

    function create_user_profile()
    {
        $id = $this->session->userdata('id');

        $data1 = array(
            'id' => $id,
            'fname' => $this->input->post('fname'),
            'sname' => $this->input->post('sname'),
            'wannabee' => $this->input->post('wannabee'),
            'specialty' => $this->input->post('specialty'),
            'website' => $this->input->post('website'),
            'phone' => $this->input->post('phone'),
            'about' => $this->input->post('editor1')
        );

        $data2 = array('email' => $this->input->post('email'));
        $data3['user_id'] = $id;

        $this->db->where('px_profiles.id', $id);
        $this->db->insert('px_profiles', $data1);

        $this->db->where('px_users.id', $id);
        $this->db->insert('px_users', $data2);

        $this->db->insert('px_files', $data3);

        redirect('/profile/edit');
    }

    function update_user_profile()
    {
        //$id = $this->session->userdata('id');
        $id = CURRENT_USER_ID;

        $this->load->helper('string');

        $config['upload_path'] = UPLOADS_PATH;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        //$this->upload->initialize($config);

        if($this->upload->do_upload('image')) {
            $file_data = $this->upload->data();

        } else {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('main_view', $error);
        }

        $data = array(
            'fname' => $this->input->post('fname'),
            'sname' => $this->input->post('sname'),
            'wannabee' => $this->input->post('wannabee'),
            'specialty' => $this->input->post('specialty'),
            'website' => $this->input->post('website'),
            'phone' => $this->input->post('phone'),
            'about' => $this->input->post('editor1'),
        );

        $data2 = array(
            'email' => $this->input->post('email')
        );

        $data3['filename'] = '../' . UPLOADS_PATH . $file_data['file_name'];

        $this->db->from('px_profiles');
        $this->db->where('id', $id);

        $this->db->set($data);
        $this->db->update();

        $this->db->from('px_users');
        $this->db->where('id', $id);

        $this->db->set($data2);
        $this->db->update();

        $this->db->from('px_files');
        $this->db->where('user_id', $id);

        $this->db->set($data3);
        $this->db->update();

        //$this->db->join('px_files', 'px_files.user_id = px_users.id');

        redirect('profile');
    }

    function check_user($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('px_profiles');
        if($query->num_rows() > 0)
        {
            $this->update_user_profile();
        } else {
            $this->create_user_profile();
        }
    }

    /*
     *  Topic Functions
     */

    function create_topic()
    {
        $id = $this->session->userdata('id');
        $data = array(
            'title' => $this->input->post('title'),
            'body' => $this->input->post('editor1'),
            'user_id' => $id
        );

        $this->db->insert('px_topics', $data);

        redirect('add/topic');
    }

    function display_topics()
    {
        $this->db->select('*');
        $this->db->from('px_topics');
        $this->db->order_by('date', 'desc');

        $query = $this->db->get();

        return $query->result_array();
    }

    function get_topic($id)
    {
        $this->db->select('*');
        $this->db->from('px_topics');
        $this->db->where('id', $id);

        $query = $this->db->get();

        return $query->row_array();
    }

    /*
     *  Event Functions
     */

    function create_event()
    {
        $id = $this->session->userdata('id');
        $month = date('Y-m-d', strtotime($this->input->post('date')));
        $time = date('H:i:s', strtotime($this->input->post('time')));
        $date = $month . ' ' . $time;

        $data = array(
            'title' => $this->input->post('title'),
            'date' => $date,
            'location' => $this->input->post('loc'),
            'body' => $this->input->post('editor1'),
            'priority' => $this->input->post('priority'),
            'user_id' => $id
        );

        $this->db->insert('px_events', $data);

        redirect('add/event');
    }

    function display_events()
    {
        $this->db->select('*');
        $this->db->from('px_events');
        $this->db->order_by('date', 'desc');

        $query = $this->db->get();

        return $query->result_array();
    }

    function get_event($id)
    {
        $this->db->select('*');
        $this->db->from('px_events');
        $this->db->where('id', $id);

        $query = $this->db->get();

        return $query->row_array();
    }

    /*
     *  Admin Functions
     */

    function create_user()
    {
        $this->load->helper('string');

        $username = $this->security->xss_clean($this->input->post('username'));
        $email = $this->security->xss_clean($this->input->post('email'));
        $password = random_string('alnum', 10);
        $pass = sha1($password);

        $data = array(
            'username' => $username,
            'password' => $pass,
            'email' => $email,
            'role' => $this->input->post('role')
        );

        $this->db->insert('px_users', $data);

        $this->db->from('px_users');
        $this->db->where('username', $username);

        $query = $this->db->get();
        $row = $query->row();
        $id = $row->id;
        $user['id'] = $id;

        $this->db->insert('px_profiles', $user);

        $image['user_id'] = $id;
        $this->db->insert('px_files', $image);

        $subject = 'Your New Password';
        $message = 'Your new passwords is' . $password;

        send_email($email, $subject, $message);

        //$subject = 'Your New Password';
        //$message = 'Your new passwords is' . $password;
        //
        //mail($email, $subject, $message);

        //$this->load->helper('email');

        //$config['protocol'] = 'sendmail';
        //$config['mailpath'] = 'C:\xampp\sendmail';
        //$config['charset'] = 'iso-8859-1';
        //$config['wordwrap'] = TRUE;
        //
        //$this->email->initialize($config);

        //$this->email->from('webmaster@projectx.dev', 'Kevin Remisoski');
        //$this->email->to($email);
        //$this->email->subject('Your new password');
        //$this->email->message('Hello '. $username . 'Your new password is' . $password);
        //$this->email->send();
        //echo $this->email->print_debugger();
    }

    function list_users()
    {
        $this->db->select('username, email, px_roles.role');
        $this->db->from('px_users');
        $this->db->join('px_roles', 'px_roles.id = px_users.role');
        $this->db->order_by('px_roles.role', 'asc');

        $query = $this->db->get();
        return $query;
    }
}