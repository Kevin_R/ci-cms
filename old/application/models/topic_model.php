<?php

class Topic_Model extends MY_Model {

    public $db_table_name = 'px_topics';
    public $_table = 'px_topics';
    protected $primary_key = "id";
    protected $fields = array("id", "user_id", "title", "body", "date");

    function __construct()
    {
        parent::__construct();
    }

    public function record_count()
    {
        return $this->db->count_all($this->db_table_name);
    }

    public function get_all_paged($limit, $offset)
    {
        $this->db->limit($limit, $offset);
        $this->db->order_by("date", "desc");
        $query = $this->db->get($this->db_table_name);

        if($query->num_rows() > 0) {
            foreach($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return FALSE;
    }

    public function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('px_topics');
        $this->db->where('id', $id);

        $query = $this->db->get();

        return $query->row_array();
    }
}