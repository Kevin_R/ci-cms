<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_Model extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_blog_entries()
	{
		$args = array('post_type' => 'post', 'post_status' => 'publish');
		return $this->get_many_by($args);
		// return $this->where('post_type', 'post')->where('post_status', 'publish')->get_all;
	}

}

/* End of file  */
/* Location: ./application/models/ */