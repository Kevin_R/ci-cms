<?php

class Members_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function validate($username, $password)
    {
        //grab user input
        $this->db->from('px_users');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->join('px_profiles', 'px_users.id = px_profiles.id');

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            $row = $query->row();
            $data = array(
                'username' => $row->username,
                'logged_in' => TRUE,
                'id' => $row->id,
                'role' => $row->role,
                'fname' => $row->fname,
                'email' => $row->email
            );
            $this->session->set_userdata($data);
            return true;
        }

        return false;
    }
}