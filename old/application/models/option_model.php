<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Option_Model extends MY_Model {

	// protected $_prefix = 'wp';

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_title()
	{
		return $this->get_by('option_name', 'blogname')->option_value;
	}

}

/* End of file  */
/* Location: ./application/models/ */