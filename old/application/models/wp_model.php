<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WP_Model extends MY_Model {

	public $variable;

	public function __construct()
	{
		// $this->helpers = array('url');
		parent::__construct();
	}

	public function get_site_url()
	{
		$this->load->helper('url');
		return site_url();
	}



}

/* End of file  */
/* Location: ./application/models/ */