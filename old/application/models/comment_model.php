<?php

class Comment_Model extends MY_Model {

    protected $before_get = '';

    function __construct()
    {
        parent::__construct();
    }

    public function get_comments()
    {
        $this->db->select('*');
        $this->db->from('px_comments');
        $this->db->join('px_topics', 'px_topics.id = px_comments.topic_id');
        $this->db->join('px_users', 'px_users.id = px_comments.user_id');
        $query = $this->db->get();
        return $query;
    }

    public function insert_comment($topic_id)
    {
        $user_id = $this->session->userdata('id');

        $data = array(
            'user_id' => $id,
            'content' => $this->input->post('content'),
            'topic_id' => $topic_id
        );

        $this->db->insert('px_comments', $data);
    }
}