<!--BEGIN CONTENT-->
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row">
            <div class="span12">
                <h3><strong>Create an event</strong></h3>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span12">
                <form action="/add/new-event" method="post" id="upload_file" accept-charset="utf-8">
                    <div class="row"><!--BEGIN INNER ROW-->
                        <div class="span6"><!--BEGIN COL1-->
                            <label for="title">Title of the event</label>
                            <input type="text" name="title" value="" class="span6"  />

                            <label for="date">Date & Time</label>
                            <div class="row">
                                <div class="span3">
                                    <input type="text" name="date" value="" class="datepicker span3"  />
                                </div>
                                <div class="span3">
                                    <input type="text" name="time" value="" class="timepicker span3"  />
                                </div>
                            </div>

                            <label for="loc">Location name</label>
                            <input type="text" name="loc" value="" class="span6"  />

                            <label for="editor1">Event description</label>
                            <div class="row">
                                <div class="span6">
                                    <textarea style="width: 50%" name="editor1"></textarea>
                                </div>
                            </div>
                        </div><!--END COL1-->
                        <div class="span6"><!--BEGIN COL2-->
                            <?=$table;?>
                            <div class="row">
                                <div class="span6">
                                    <h3>Event image</h3>
                                    <img width="100%" height="100%" src="<?= ph_img(570,247);?>" alt="" /><br />
                                </div>
                            </div>
                            <div class="row">
                                <div class="span2 offset4">
                                    <button style="float: right" class="btn">
                                        Upload image
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span6">
                                    <div class="row">
                                        <div class="span2">
                                            <label for="priority">Priority:</label>
                                        </div>
                                        <div class="span4">
                                            <select name="priority">
                                                <option value="highest">Highest</option>
                                                <option value="normal">Normal</option>
                                                <option value="lowest">Lowest</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3 offset3">
                                            <button style="float: right" class="btn">Create event</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--END COL2-->
                    </div><!--END INNER ROW-->
                </form>
            </div>
        </div><!--END ROW-->
    </div><!--END CONTENT CONTAINER-->
<!--END CONTENT-->