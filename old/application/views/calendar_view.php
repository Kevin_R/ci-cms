<!--BEGIN CONTENT-->
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span3">
                <h3><?=date('F');?></h3>
            </div>
            <div class="span3 offset6">
                <button class="btn">&lt;&lt;&nbsp;History</button>
                <a href="add/event" class="btn">Create event</a>
            </div>
        </div><!--END ROW-->
        <?php foreach($events as $event): ?>
        <?php

        $datetime = strtotime($event->date);
        $mdate = strtolower(date('d F', $datetime));
        $mtime = date('H:i', $datetime);

        ?>

        <div class="row"><!--BEGIN EVENT ROW1-->
            <div class="span3">
                <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                    <p>
                        <strong><?=$event->title;?></strong><br />
                        <em>Topic name</em>
                    </p>
                </img>
            </div>
            <div class="span2 offset6">
                <p class="text-right"><strong><?=$mdate;?></strong></p>
            </div>
            <div class="span1">
                <p class="text-right"><strong class="text-right"><?=$mtime;?></strong></p>
            </div>
        </div><!--END ROW-->
        
        <?php endforeach ?>
        <div class="row"><!--BEGIN ROW-->
             <div class="span3">
                <h3>December</h3>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN EVENT ROW1-->
            <div class="span3">
                <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                    <p>
                        <strong>Event name</strong><br />
                        <em>Topic name</em>
                    </p>
                </img>
            </div>
            <div class="span2 offset6">
                <p class="text-right"><strong>21 december</strong></p>
            </div>
            <div class="span1">
                <p class="text-right"><strong class="text-right">23:59</strong></p>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN EVENT ROW2-->
            <div class="span3">
                <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                    <p>
                        <strong>Event name</strong><br />
                        <em>Topic name</em>
                    </p>
                </img>
            </div>
            <div class="span2 offset6">
                <p class="text-right"><strong>21 december</strong></p>
            </div>
            <div class="span1">
                <p class="text-right"><strong class="text-right">23:59</strong></p>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN EVENT ROW3-->
            <div class="span3">
                <img style="float: left;padding-right: 10px" src="<?= ph_img(100,45);?>" alt="">
                    <p>
                        <strong>Event name</strong><br />
                        <em>Topic name</em>
                    </p>
                </img>
            </div>
            <div class="span2 offset6">
                <p class="text-right"><strong>21 december</strong></p>
            </div>
            <div class="span1">
                <p class="text-right"><strong class="text-right">23:59</strong></p>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN EVENT ROW4-->
            <div class="span3">
                <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                    <p>
                        <strong>Event name</strong><br />
                        <em>Topic name</em>
                    </p>
                </img>
            </div>
            <div class="span2 offset6">
                <p class="text-right"><strong>21 december</strong></p>
            </div>
            <div class="span1">
                <p class="text-right"><strong class="text-right">23:59</strong></p>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span12">
                <img src="<?=ph_img(1170,150);?>" alt="" />
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span12">
                <p>
                    <em>48 pirates on board!</em>
                    <em>~ 22 posts</em>
                    <em>~ 3 videos</em>
                    <em>~ 13 files</em>
                </p>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN ROW-->
             <div class="span3">
                <h3>January</h3>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span3">
                <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                    <p>
                        <strong>Event name</strong><br />
                        <em>Topic name</em>
                    </p>
                </img>
            </div>
            <div class="span2 offset6">
                <p class="text-right"><strong>1 january</strong></p>
            </div>
            <div class="span1">
                <p class="text-right"><strong class="text-right">18:00</strong></p>
            </div>
        </div><!--END ROW-->
    </div><!--END CONTENT CONTAINER-->
<!--END CONTENT-->