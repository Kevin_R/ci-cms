<!DOCTYPE HTML><!--BEGIN HEADER-->
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
    <link rel="stylesheet" href="<?= base_url() . CSS_PATH . 'style.css';?>" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />
</head>
<body>
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span1">
                <img src="<?=ph_img(90,50);?>" alt="" />
            </div>
            <div class="span4">
                <ul id="my-main-nav" class="nav nav-pills">
                    <!--<li class="active">-->
                    <li>
                        <a href="/"><i class="icon-home"></i>&nbsp;&nbsp;Home</a>
                    </li>
                    <li>
                        <a href="/calendar"><i class="icon-calendar"></i>&nbsp;&nbsp;Calendar</a>
                    </li>
                    <li>
                        <a href="/topics"><i class="icon-list-ul"></i>&nbsp;&nbsp;Topics</a>
                    </li>
                </ul>
            </div>
            <div class="span3 offset4">
                <ul class="nav nav-pills">
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            <?=CURRENT_USERNAME;?>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/add/topic">New topic</a></li>
                            <li><a href="/add/event">New event</a></li>
                            <li><a href="/profile">My profile</a></li>
                            <?php if(CURRENT_ROLE == 1) : ?>
                            <li><a href="/admin">Admin</a></li>
                            <?php endif ?>
                        </ul>
                    </li>
                    <li><a href="/logout">uitloggen</a></li>
                </ul>
            </div>
        </div><!--END ROW-->
    </div><!--END CONTIANER-->
<!--END HEADER-->