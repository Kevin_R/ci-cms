<!--BEGIN CONTENT-->
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span12">
                <h3>Profile of <?=$query['fname'] . '&nbsp;' . $query['sname'];?></h3>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span4">
                <img width="100%" height="100%" src="<?=$query['filename'];?>" alt="" />
            </div>
            <div class="span4">
                <h3 style="margin: 0;padding: 0">Passport</h3>
                    <address>
                        Name: &nbsp;<?=$query['fname'];?>&nbsp;<?=$query['sname'];?><br />
                        Wannabee: &nbsp;<?=$query['wannabee'];?><br />
                        Specialty: &nbsp;<?=$query['specialty'];?><br />
                        Web: &nbsp;<?=$query['website'];?><br />
                        Phone: &nbsp;<?=$query['phone'];?><br />
                        E-mail: &nbsp;<?=$query['email'];?><br />
                    </address>
                <h3>About <?=$query['fname'];?></h3>
                <p><?=$query['about'];?></p>
                <?php if($username == $query['username']): ?>
                <a href="/profile/edit">edit</a>
                <?php endif; ?>
            </div>
            <div class="span4">
                <h3><?=$query['fname'];?>&nbsp;will be present @</h3>
                <div class="row"><!--BEGIN EVENT1-->
                    <div class="span3">
                        <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                            <p>
                                <strong>21 December</strong><br />
                                <strong>Event name</strong><br />
                            </p>
                        </img>
                    </div><!--END SPAN3-->
                    <div class="span1">
                        <p class="text-right"><strong class="text-right">23:59</strong></p>
                    </div>
                </div><!--END EVENT1-->
                <div class="row"><!--BEGIN EVENT1-->
                    <div class="span3">
                        <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                            <p>
                                <strong>21 December</strong><br />
                                <strong>Event name</strong><br />
                            </p>
                        </img>
                    </div><!--END SPAN3-->
                    <div class="span1">
                        <p class="text-right"><strong class="text-right">23:59</strong></p>
                    </div>
                </div><!--END EVENT1-->
                <div class="row"><!--BEGIN EVENT1-->
                    <div class="span3">
                        <img style="float: left;padding-right: 10px" src="<?= ph_img(50,40);?>" alt="">
                            <p>
                                <strong>21 December</strong><br />
                                <strong>Event name</strong><br />
                            </p>
                        </img>
                    </div><!--END SPAN3-->
                    <div class="span1">
                        <p class="text-right"><strong class="text-right">23:59</strong></p>
                    </div>
                </div><!--END EVENT1-->
            </div>
        </div><!--END ROW-->
        <div class="row">
            <div class="span12">
                <h3><?=$query['fname'];?>'s topics</h3>
            </div>
        </div><!--END ROW-->
        <div class="row">
            <div class="span12">
                <div class="post rounded10-solid">
                    <h4>Subject topic begin</h4>
                    <img style="float: left; padding-right: 10px" src="' . ph_img(110,75) . '" alt="" >
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget orci nec velit sollicitudin adipiscing nec et odio. Mauris ut nibh ac ante elementum eleifend cursus vitae nisl. Sed mauris purus, cursus a semper nec, aliquet vel metus. Quisque erat libero, sodales vel est vel, pellentesque rutrum nibh. Nulla dictum leo consectetur, tempor orci a, malesuada lacus. Cras et massa pulvinar dui vehicula mattis. Nunc sed velit et nunc consequat convallis. Nullam ipsum elit, volutpat nec est in, dictum mattis enim. Suspendisse nec purus vitae lectus mollis placerat.
                            <span><a style="text-decoration:underline; float: right" href="#">read on...</a></span>
                        </p>
                    </img>
                </div>
                <div class="row">
                    <div class="span2 offset2">
                        <p class="text-center">last action: 21 april</p>
                    </div>
                    <div class="span2">
                        <p class="text-center">38 posts total</p>
                    </div>
                    <div class="span2">
                        <p class="text-center">12 videos and pics</p>
                    </div>
                    <div class="span2">
                        <p class="text-center">
                            next event: 39 april
                        </p>
                    </div>
                </div>
            </div>
        </div><!--END ROW-->
        <div class="row">
            <div class="span12">
                <h3><?=$query['fname'];?>'s posts</h3>
            </div>
        </div><!--END ROW-->
        <div class="row">
            <div class="span4">
                <img style="float: left;padding-right: 10px" src="<?=ph_img(45);?>" alt="">
                    <p>
                        <strong>Maya Link</strong><br />
                        lorem ipsum i like it a lot
                    </p>
                </img>
                <div class="row">
                    <div class="span3 offset1">
                        <img style="float: left;padding-right: 10px" src="<?=ph_img(45);?>" alt="">
                            <p>
                                <strong>Daan A</strong><br />
                                lorem ipsum i like it a lot
                            </p>
                        </img>
                    </div>
                </div><!--END INNER ROW-->
                <div class="row">
                    <div class="span3 offset1">
                        <img style="float: left;padding-right: 10px" src="<?=ph_img(45);?>" alt="">
                            <p>
                                <strong>Daan A</strong><br />
                                lorem ipsum i like it a lot
                            </p>
                        </img>
                    </div>
                </div><!--END INNER ROW-->
            </div>
        </div><!--END ROW-->
    </div><!--END CONTENT CONTAINER-->
<!--END CONTENT-->