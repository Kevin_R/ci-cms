<div class="container"><!--BEGIN CONTENT-->
    <div class="row"><!--begin row-->
        <div class="span6">
            <h3>Topics</h3>
        </div><!--end span6-->
        <div class="span2 offset4">
            <a href="add/topic" class="btn">Create topic</a>
        </div><!--end span2-->
    </div><!--end row-->
    <div class="listing">
        <!--<div class="span12">-->
        <?php foreach($topics as $topic) : ?>
        <?php $datetime = strtotime($topic->date); ?>
        <?php $mdate    = strtolower(date('d F', $datetime)); ?>
        <?php $mtime    = date('H:i', $datetime); ?>
        <div class="row">
        <div class="span12">
            <div style="margin-bottom: 20px;" class="topics">
                <div class="topic rounded10-solid">
                    <h4><?=$topic->title;?></h4>
                    <img style="float: left; padding-right: 20px" src="<?=ph_img(110,75);?>" alt="" />
                    <?=word_limiter($topic->body, 75);?> <br />
                    <span class="offset9" style="float: right"><a href="#">read on...</a></span>
                    <div style="clear: both"></div>
                </div><!--end topic-->
                <div class="row">
                    <div class="span2 offset2">
                        <p class="text-center">last action: <?=$mdate;?></p>
                    </div><!--end span2-1-->
                    <div class="span2">
                        <p class="text-center">38 posts total</p>
                    </div><!--end span2-2-->
                    <div class="span2">
                        <p class="text-center">12 videos and pics</p>
                    </div><!--end span2-3-->
                    <div class="span2">
                        <p class="text-center">next event: 39 april</p>
                    </div><!--end span2-4-->
                </div><!--end row-->
            </div>
        </div>
        </div>
        <?php endforeach; ?>
        <?=$links?>
    </div><!--end row.listing-->
</div><!--END CONTENT-->