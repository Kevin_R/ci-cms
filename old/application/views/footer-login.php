<!--SCRIPTS-->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?=JS_PATH.'bootstrap.js';?>"></script>
    <script type="text/javascript" src="<?=JS_PATH.'ckeditor/ckeditor.js';?>"></script>
    <script type="text/javascript" src="<?=JS_PATH.'jqdrawbsgrid.js';?>"></script>
    <script type="text/javascript" src="<?=JS_PATH. 'jquery-ias.min.js';?>"></script>
    <script type="text/javascript">
        //jQuery(".container").drawBootstrapGrid();
    </script>
    <script type="text/javascript">
        CKEDITOR.replace('editor1', {
            customConfig: 'custom-config.js'
        });
    </script>
    <script type="text/javascript">
        jQuery.ias({
            container: '.listing',
            item: '.post',
            loader: '<img src="<?php get_loader();?>"/>'
        })
    </script>
<!--END SCRIPTS-->
</body>
</html>