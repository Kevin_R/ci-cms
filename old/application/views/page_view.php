<!--BEGIN CONTENT-->
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row">
            <div class="span8">
                <h2><?= $query['title']; ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <img style="float: left; padding-right: 20px" src="<?=ph_img(125,100);?>" alt="" />
                <p>
                    <?= $query['body']; ?>
                </p>
            </div>
        </div><!--END ROW-->
    </div><!--END CONTENT CONTAINER-->
<!--END CONTENT-->