<!--BEGIN CONTENT-->
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row">
            <div class="span12">
                <h2><strong>Hi&nbsp;<?=$query['fname'];?>, edit your visuml</strong></h2>
            </div>
        </div>
        <div class="row">
            <!--<div class="span4">-->
                <?=form_open_multipart('profile/update');?>
                <div class="span4">
                    <fieldset>
                        <label for="fname" class="my-bold-text">Name</label>
                        <input type="text" name="fname" value="<?=$query['fname'];?>"  />
                        <label for="sname" class="my-bold-text">Sirname</label>
                        <input type="text" name="sname" value="<?=$query['sname'];?>"  />
                        <label for="wannabee" class="my-bold-text">Wannabee</label>
                        <input type="text" name="wannabee" value="<?=$query['wannabee'];?>"  />
                        <label for="specialty" class="my-bold-text">Specialty</label>
                        <input type="text" name="specialty" value="<?=$query['specialty'];?>"  />
                        <label for="website" class="my-bold-text">Website</label>
                        <input type="text" name="website" value="<?=$query['website'];?>"  />
                        <label for="email" class="my-bold-text">E-mail</label>
                        <input type="text" name="email" value="<?=$query['email'];?>"  />
                        <label for="phone" class="my-bold-text">Phone</label>
                        <input type="text" name="phone" value="<?=$query['phone'];?>"  />
                        <label for="editor1" class="my-bold-text">About me:</label>
                        <textarea name="editor1" cols="40" rows="10" ><?=$query['about'];?></textarea><br />
                </div>
                <div class="span4">
                    <p class="my-bold-text">Your foto</p>
                    <img src="<?=$query['filename'];?>" alt="" /><br /><br />
                    <?php $atts = array(
                        'value' => 'Upload image',
                        'name' => 'image',
                        'class' => 'btn-small'

                    );
                    ?>
                    <?= form_upload($atts);?>
                    <button name="submit" type="submit" class="btn" >Update profile</button>
                </div>
                    </fieldset>
                </form>
                <div class="span3 offset1">
                <h2><strong>Passport</strong></h2>
                <address>
                    Name: <?=$query['fname'];?>&nbsp;<?=$query['sname'];?><br />
                    Wannabee: <?=$query['wannabee'];?><br />
                    Specialty: <?=$query['specialty'];?><br />
                    Web: <?=$query['website'];?><br />
                    Phone: <?=$query['phone'];?><br />
                    E-mail: <?=$query['email'];?>
                </address>
                <h3><strong>About <?=$query['fname'];?>:</strong></h3>
                <p><?=$query['about'];?></p>
            </div>
            </div>
        </div><!--END ROW-->
    </div><!--END CONTENT CONTAINER-->
<!--END CONTENT-->