<?php

$jq_ver = '1.9.1';
$jqui_ver = '1.10.3';
$dt_ver = '1.9.4';

?>
<!--BEGIN FOOTER-->
    <div class="container"><!--BEGIN FOOTER CONTAINER-->
        <div class="row">
            <footer class="span12">
                <small>
                    <p>Copywrong by the Drunken Fly | Made possible by Eratosthenes and Pythagoras</p>
                </small>
            </footer>
        </div>
    </div><!--END FOOTER CONTAINER-->

<!--SCRIPTS-->
    <!--[if lt IE 9]>
        <script src="<?=base_url().JS_PATH .'html5shiv.js';?>" type="text/javascript"></script>
    <![endif]-->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/<?=$jq_ver;?>/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/<?=$jqui_ver;?>/jquery-ui.js"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH.'bootstrap.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH.'ckeditor/ckeditor.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH.'jqdrawbsgrid.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'jquery.infinitescroll.min.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'jquery-ias.min.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'jquery.equalheights.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'picker.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'picker.date.js';?>"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'picker.time.js';?>"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/<?=$dt_ver;?>/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'dataTables.bootstrapPagination.js';?>"></script>
    <script type="text/javascript">
        //jQuery(".container").drawBootstrapGrid();
    </script>
    <script type="text/javascript" src="<?=base_url().JS_PATH. 'scripts.js';?>"></script>
    <script type="text/javascript">
        $('.listing').infinitescroll({
            loading: {
            img: "<?php get_loader();?>",
            speed: "slow"
            },
            navSelector: ".pagination",
            nextSelector: "li.scroll > a",
            itemSelector: ".topics"
            //debug: true
        });
    </script>
<!--END SCRIPTS-->
</body>
</html>