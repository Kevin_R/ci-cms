<div class="container">
    <div class="row">
        <div class="span3">
            <h1>Admin Area</h1>
        </div>
        <div class="row">
            <div class="span4 offset1">
                <h2>Add User</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="span6 offset3 form-admin">
            <?=form_open('admin/add-user');?>
            <?=form_label('Username:', 'username');?>
            <?=form_input('username');?>
            <?php
                $options = array(
                    '3' => 'User',
                    '2' => 'Moderator',
                    '1' => 'Administrator'
                );
            ?>
            <?=form_label('Role:', 'role');?>
            <?=form_dropdown('role', $options);?>
            <?=form_label('Email:', 'email');?>
            <?=form_input('email');?>
            <?php
                $atts = array(
                    'class' => 'btn',
                    'name' => 'submit',
                    'value' => 'Create User'
                );
            ?>
            <br />
            <?=form_submit($atts);?>
            <?=form_close();?>
        </div>
    </div>
</div>