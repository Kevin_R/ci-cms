<?php get_header('login'); ?>
<!--BEGIN CONTENT-->
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="span6 offset3">
                    <h1>At World's End</h1>
                </div>                
            </div>
        </div>
    </div>
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row">
            <div class="span12">
                <div id="login-container" class="span6 offset3">
                    <div class="span3 offset3">
                        <?php
                        $atts1 = array(
                            'placeholder' => 'Name'
                        );
                        $atts2 = array(
                            'placeholder' => '********'
                        );
                        
                        echo form_open('login/validate_cred');
                        echo form_label('Username', 'username');
                        echo form_input('username', $atts1['placeholder']);
                        echo form_label('Password', 'password');
                        echo form_password('password', $atts2['placeholder']);
                        echo '<br />';
                        echo form_submit('submit', 'Login');
                        echo form_close();    
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!--END CONTENT CONTAINER-->
<!--END CONTENT-->
<?php get_footer('login'); ?>