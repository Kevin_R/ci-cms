<!--BEGIN CONTENT-->
    <div class="container"><!--BEGIN CONTAINER-->
        <div class="row">
            <div class="span12">
                <h3><strong>Create a topic</strong></h3>
            </div>
        </div><!--END ROW-->
        <div class="row"><!--BEGIN ROW-->
            <div class="span12">
                <form action="/add/new-topic" method="post" accept-charset="utf-8">
                    <div class="row"><!--BEGIN INNER ROW-->
                        <div class="span6"><!--BEGIN COL1-->
                            <label for="title">Title of the topic</label>
                            <input type="text" name="title" value="" class="span6"  />
                            
                            <label for="editor1">Topic description</label>
                            <div class="row">
                                <div class="span6">
                                    <textarea style="width: 50%" name="editor1"></textarea>
                                </div>
                            </div>
                        </div><!--END COL1-->
                        <div class="span6"><!--BEGIN COL2-->
                            <table class="table">
                                <tr>
                                    <td>Function</td>
                                    <td>Zoe</td>
                                    <td>moderator</td>
                                </tr>
                                <tr>
                                    <td>Function</td>
                                    <td>Daan</td>
                                    <td>moderator</td>
                                </tr>
                                <tr>
                                    <td>Function</td>
                                    <td>Jim</td>
                                    <td>authorize</td>
                                </tr>
                                <tr>
                                    <td>Function</td>
                                    <td>Erik</td>
                                    <td>moderator</td>
                                </tr>
                            </table>
                            <div class="row">
                                <div class="span6">
                                    <h3>Topic image</h3>
                                    <img width="100%" height="100%" src="<?= ph_img(570,247);?>" alt="" /><br />
                                </div>
                            </div>
                            <div class="row">
                                <div class="span2 offset4">
                                    <button style="float: right" class="btn">
                                        Upload image
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span6">
                                    <table class="table">
                                        <tr>
                                            <td>Sunrise</td>
                                            <td>23 dec</td>
                                            <td>23:59</td>
                                        </tr>
                                        <tr>
                                            <td>Truth or dare</td>
                                            <td>31 dec</td>
                                            <td>23:59</td>
                                        </tr>
                                    </table>
                                    <div class="row">
                                        <div class="span3 offset3">
                                            <button style="float: right" class="btn">Create topic</button>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div><!--END COL2-->
                    </div><!--END INNER ROW-->
                </form>
            </div>
        </div><!--END ROW-->
    </div><!--END CONTENT CONTAINER-->
<!--END CONTENT-->