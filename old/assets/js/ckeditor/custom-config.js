/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
        config.toolbarGroups = [
            { name: 'document', groups: [ 'mode' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo'] },
            { name: 'links' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup'] },
            { name: 'paragraphs', groups: [ 'list', 'indent', 'blocks', 'align' ]},
            { name: 'insert' },
            '/',
            { name: 'styles' }
        ];
};
