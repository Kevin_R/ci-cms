$(document).ready(function(){
    $(".my-col").equalHeights();
});

$(function(){
    function stripTrailingSlash(str) {
        if(str.substr(-1) == '/') {
        return str.substr(0, str.length - 1);
        }
    return str;
    }

    var url = window.location.pathname;
    var activePage = stripTrailingSlash(url);

    $('#my-main-nav li a').each(function(){
        var currentPage = stripTrailingSlash($(this).attr('href'));

        if (activePage == currentPage) {
            $(this).parent().addClass('active');
        }
    });
});

$('.datepicker').pickadate();

$('.timepicker').pickatime({
    interval: 15,
    format: 'HH:i'
});

$(document).ready(function() {
    $('#example').dataTable({
        "iDisplayLength": 5,
        "bJQueryUI" : true,
        "aaSorting": [[ 4, "desc"]],
        "sPaginationType": "bootstrap"
    });
});

CKEDITOR.replace('editor1', {
    customConfig: 'custom-config.js'
});