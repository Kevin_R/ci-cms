# CI-CMS

Developed by Kevin Remisoski

The goal of this CMS currently, is to utilize one of the greatest frameworks out there with one of the best CMS platforms.  This started out as a CodeIgniter project and while it was on the backburner for a little while, with the combination of Bonfire, Bootstrap, and Wordpress as well as another project I plan on merging into the code soon, as well as the social networking platform I'd already developed (merges coming soon since I've upgraded to Bonfire and CI3), it is my hope to not only have a robust web and web app development service, but also a lightning fast one that will pick up some of the slack where Wordpress has fallen short.

I"m working hard to make sure that the majority of the Wordpress codex can be reused on this platform and of course I urge you to try out the very cool MY_Model and MY_Controller that come stock with Bonfire.

#New Features coming soon!

* Full integration with WordPress
* A new blazing fast CSS and JS compiler to speed up your site and CDN ready.
* Superior caching.
* Social Features

Anyone that would like to help, let me know.  I'm doing the best with what little time I have these days, but am looking forward to integrating more frameworks and more popular CMS's when the time comes.

# Faster Development of CodeIgniter Apps

<div style="float: right; margin: 0 20px 20px 0">
	<a href='http://www.pledgie.com/campaigns/15326'><img alt='Click here to lend your support to: Bonfire - faster CodeIgniter development and make a donation at www.pledgie.com !' src='http://pledgie.com/campaigns/15326.png?skin_name=chrome' border='0' /></a>
</div>

That's Bonfire's goal: provide a solid base off of which to build your new web applications.
It's not a CMS.
Instead, it's a springboard to build off of with many of the tools you wish you had on projects but never took the time to build.

All wrapped up in an elegant interface that will make you proud when you hand the project over to your client.

## Current Features

- CodeIgniter 3.x included. CodeIgniter 2.x support available with [some minor changes](https://github.com/ci-bonfire/Bonfire/blob/develop/bonfire/docs/ci2.md).
- Very flexible template/theme system, capable of Wordpress-like parent/child themes.
- Uses Twitter Bootstrap 2.x for the Admin and Default themes
- Fully modular and built around HMVC
- 4 ‘contexts’ ready for your code: Content, Reports, Settings and Developer Tools
- Database backup and maintenance interface
- Role-based access control
- Built-in users/auth system
- Code Builder with CRUD generation
- Simple email queue system
- 2-step installer
- Uses multiple-environment config files.
- Migration-ready (using either raw SQL or Database Forge commands)
- Log view/maintenance

## Road Map

Check out our online [road map](https://trello.com/board/bonfire-roadmap/4f21de254768c8463f09c85b) where you can comment and vote on items in the lists.

## Stay Up To Date

Follow Bonfire's progress and commits at Twitter by following [cibonfire](http://twitter.com/#!/cibonfire).

## Lend A Hand

If you're interested in helping out, fork the project and start coding! I'd love to have you on board. You can always shoot me an email at lonnieje@gmail.com and we can talk about how you'll best fit it and what the best place to start coding would be.

Let's make this the best kick-start to any CodeIgniter project.

### Branches

Due to the shift from CI2 to CI3, there are 2 branches containing recent releases and 2 development branches.

- The releases are in the master (v0.8.x) and 0.7.x (v0.7.x) branches.
- The development branches are develop (v0.8.x) and 0.7-dev (v0.7.x) branches.

Any changes due to issues in the Bonfire code are currently being made to both development branches.

- v0.8.x uses CodeIgniter 3 by default. CI2 is supported, but not included.
- v0.7.x uses CodeIgniter 2 by default. CI3 is supported, but not included.

## Bug Reports

We strive to make Bonfire a solid base to work with. In doing so your Bug Reports are very vital to us. Before making a Bug Report please check the existing
[Issue Tracker](https://github.com/ci-bonfire/Bonfire/issues) , if the bug you have found does not exist in the issue tracker already, please text a minute to read the [guide lines](http://cibonfire.com/docs/developer/issue_tracking_and_pull_requests) to making a Good Bug report.


## The Team

The Bonfire Team is made up of developers from around the world interested in making Bonfire a better kickstart for your [CodeIgniter](http://codeigniter.com) projects.

- [Lonnie Ezell](http://lonnieezell.com) - Lead Developer
- [Mat Whitney](https://github.com/mwhitneysdsu)
- [Alan Jenkins](https://github.com/sourcejedi)

[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/ci-bonfire/bonfire/trend.png)](https://bitdeli.com/free "Bitdeli Badge").